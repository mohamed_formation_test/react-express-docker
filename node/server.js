const express = require('express')
const cors = require('cors')

const app = express()

app.use(cors())

app.get('/', (req, res) => {
  res.json([
   {
      "id":"1",
      "title":"Annonce 1 : Le developpement, c'est vraiment top !!"
    },
    {
      "id":"2",
      "title":"Annonce 2 : La formation est vraiment super !!"
    },
    {
      "id":"3",
      "title":"Annonce 3 : C'est bientôt la pause, non??"
    }
  ])
})

app.listen(4000, () => {
  console.log('connected on port 4000')
})